//alert("Hello, Batch 241!");

//ARITHMETIC OPERATORS
let x = 1397;
let y = 7831;

//Addition Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

//Subtraction Operator
let difference = x - y;
console.log("Result of the subtraction operator: " + difference); 

//Multiplication Operator
let product = x * y;
console.log("Result of the multiplication operator: "+ product);

//Division Operator
let quotient = x / y;
console.log("Result of the division operator: " + quotient);

//Modulo Operator
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


//ASSIGNMENT OPERATOR
//Basic Assignment Operator(=)
//The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

//Addition Assignment Operator
//Uses the current value of the variable and it ADDS a number (2) to itself. Afterwards, reassigns it as a new value.
//assignmentNumber = assignmentNumber + 2
console.log(assignmentNumber);
assignmentNumber += 2;
console.log("Result of the addition assignment operator: " + assignmentNumber); //10

console.log(assignmentNumber)
//Subtraction/Multiplication/Division (-=, *=, /=)

//Subtraction Assignment Operator
assignmentNumber -= 2; // assignmentNumber = assignmentNumber - 2
console.log("Result of the subtraction assignment operator: " + assignmentNumber); //8

//Multiplication Assignment OPerator
assignmentNumber *= 2; //assignmentNumber = assignmentNumber * 2
console.log("Result of the subtraction assignment operator: " + assignmentNumber); //16

//Division Assignment Operator
assignmentNumber /= 2; //assignmentNumber = assignmentNumber / 2
console.log("Result of the division assignment operator: " +assignmentNumber); //8


//Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6


*/
//let mdas = 3 - 2.4;
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas); // 0.6000000000000001

//The order of operations can be changed by adding parenthesis to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas); //0.19999999999999996

let sample = 5 % 2 + 10;
/*
	5 % 2 = 1
	1 + 10 = 11

*/
console.log(sample); //11

sample = 5 + 2 % 10;
console.log(sample);

//INCREMENT AND DECREMENT
//Operators that add or subract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// let increment = z;
// console.log(increment);

//Pre-Increment
//The value of "z" is added by a value of 1 before returning the value and storing it in the the variable "increment";
// let preIncrement = ++z; //2
// console.log("Result of the pre-increment: " + preIncrement); //2

// console.log("Result of the pre-increment: " + z); //2

//Post-Increment
//The value of "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement); // 1
console.log("Result of the postIncrement: " + z); //2

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1
	
	Pre-decrement - subtracts 1 first before reading value
	Post-decrement - reads value first before subtracting 1
*/

let a = 2;

//Pre-Decrement
// The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement";
// let preDecrement = --a;
// console.log("Result of the pre-decrement: " + preDecrement); //1
// console.log("Result of the pre-decrement: " + a); //1

//Post-Decrement
//The value "a" is returned and stored in the variable "postDecrement" then the value of "a" is decreased by 1;
let postDecrement = a--;
console.log("Result of the postDecrement: "+ postDecrement); //2
console.log("Result of the postDecrement" + a); //1


//TYPE COERCION
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another.

*/

let numA = '10'; //String
let numB = 12; //Number

/*
	Adding/concatenating a string and a number will result as a string

*/

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //String


let coercion1 = numA - numB;
console.log(coercion1); //-2
console.log(typeof coercion); 

//Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


//Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //number

let numF = false + 1;
console.log(numF); //1


//COMPARISON OPERATOR
let juan = 'juan';

//Equality Operator(==)
/*
	-Checks whether the operands are equal/have the same content
	-Attempts to CONVERT and COMPARE operands of different data types
	-Returns a boolean value (true / false)

*/

console.log(1 == 1); //True
console.log(1 == 2); //False
console.log(1 == '1'); //True
console.log(1 == true); //True
//compare two strings that are the same
console.log('juan' == 'juan');//True
console.log('true' == true); //False
console.log(juan == 'juan'); //True

//Inequality Operator(!=)
/*
	-Checks whether the operands are not equal/have different content
	-Attempts to CONVERT AND COMPARE operands of diff data types

*/
console.log(1 != 1); //False
console.log(1 != 2); //True
console.log(1 != '1'); //False
console.log(1 != true); //False 
console.log('juan' != 'juan'); //False
console.log('juan' != juan); //False


//Strict Equality Operator(===)
/*
	-Checks whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values

*/

console.log(1 === 1); //true
console.log(1 === 2); //false 
console.log(1 === '1'); //false
console.log(1 === true); //false
console.log('juan' === 'juan'); //true
console.log(juan === 'juan'); // true

//Strict Inequality Operator (!==)
/*
	Checks whether the operands are not equal/have the same content
	Also COMPARES the data types of 2 values

*/

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1');  //true
console.log(1 !== true); //true
console.log('juan' !== 'juan'); // false
console.log(juan !== 'juan'); //false


//RELATIONAL OPERATOR
//Returns a boolean value
let j = 50;
let k = 65;

//GT/Greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan); //false

//LT/Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan); //true

//GTE /Greater than or Equal operator(>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); //false

//LTE /Less than or Equal operator(<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //true

let numStr = "30";
//forced coercion to change the string to a number
console.log(j > numStr); //true

let str = "thirty";
//(50 is greater than NaN)
console.log(j > str); //false


//LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

//Logical AND Operator (&& -Double Ampersand)
//Returns true if all operands are true
//true && true = true
//true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet); //false

//Logical OR operator (|| - Double Pipe)
// Returns true if one of the operands are true
//true || true = true
//true || false = true
//false || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //True

//Logical NOT Operator (! - Exclamation point)
//Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet); //True


//AND - all should be true ~~ true
//AND - if is false ~~ false
//OR - at least one is true ~~ true
//OR - all are false ~~ false
//(NOT true) !true = false
//(NOT false) !false = true
